<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">
            {{ config('app.name', 'Laravel') }}
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item active">
        <a href="{{ route('students.home') }}" class="nav-link">
            <i class="fa fa-bullhorn"></i>
            <span>Announcements</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-tasks"></i>
        <span>Activities</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('students.assignments') }}">Assignments</a>
                <a class="collapse-item" href="#">Attendance</a>
                <a class="collapse-item" href="#">Exams</a>
                <a class="collapse-item" href="#">Projects</a>
            </div>
        </div>
    </li>
    
    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="fa fa-book"></i>
            <span>Subjects</span>
        </a>
    </li>

    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="fas fa-coins fa-fw"></i>
            <span>Payments</span>
        </a>
    </li>

    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="fas fa-envelope fa-fw"></i>
            <span>Emails</span>
        </a>
    </li>

    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="fas fa-inbox fa-fw"></i>
            <span>Messages</span>
        </a>
    </li>

    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="fas fa-cogs fa-fw"></i>
            <span>Reports</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->