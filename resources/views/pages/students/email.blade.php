@extends('pages.students.layouts')

@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    @include('inc.students.topbar')
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Email</h1>

        <div class="email-app mb-4">
            <nav>
                <a href="page-inbox-compose.html" class="btn btn-danger btn-block">New Email</a>
                <ul class="nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><i class="fa fa-inbox"></i> Inbox <span class="badge badge-danger">4</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-rocket"></i> Sent</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-trash-o"></i> Trash</a>
                    </li>
                </ul>
            </nav>
            <main class="inbox">
                <div class="toolbar">
                    
                    <button type="button" class="btn btn-light pl-1 pr-0" data-toggle="tooltip" title="Select All">
                        <div class="custom-control custom-checkbox action ml-1">
                            <input type="checkbox" class="custom-control-input" id="email" name="email">
                            <label class="custom-control-label" for="email"></label>
                        </div>
                    </button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-light" data-toggle="tooltip" title="Mark all as read">
                            <span class="fa fa-envelope-o"></span>
                        </button>
                    </div>
                    <button type="button" class="btn btn-light" data-toggle="tooltip" title="Move to trash">
                        <span class="fa fa-trash-o"></span>
                    </button>
                    <div class="btn-group">
                        <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
                            <span class="fa fa-tags"></span>
                            <span class="caret"></span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">add label <span class="badge badge-danger"> Home</span></a>
                            <a class="dropdown-item" href="#">add label <span class="badge badge-info"> Job</span></a>
                            <a class="dropdown-item" href="#">add label <span class="badge badge-success"> Clients</span></a>
                            <a class="dropdown-item" href="#">add label <span class="badge badge-warning"> News</span></a>
                        </div>
                    </div>
                    <div class="btn-group float-right">
                        <button type="button" class="btn btn-light">
                            <span class="fa fa-chevron-left"></span>
                        </button>
                        <button type="button" class="btn btn-light">
                            <span class="fa fa-chevron-right"></span>
                        </button>
                    </div>
                </div>

                <ul class="messages">
                    <li class="message unread">
                        <a href="#">
                            <div class="actions">
                                <div class="custom-control custom-checkbox action ml-1">
                                    <input type="checkbox" class="custom-control-input" id="email_1" name="email[]">
                                    <label class="custom-control-label" for="email_1"></label>
                                </div>
                            </div>
                            <div class="header">
                                <span class="from">Lukasz Holeczek</span>
                                <span class="date">
                                <span class="fa fa-paper-clip"></span> Today, 3:47 PM</span>
                            </div>
                            <div class="title">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </div>
                            <div class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </div>
                        </a>
                    </li>
                    <li class="message">
                        <a href="#">
                            <div class="actions">
                                <div class="custom-control custom-checkbox action ml-1">
                                    <input type="checkbox" class="custom-control-input" id="email_2" name="email[]">
                                    <label class="custom-control-label" for="email_2"></label>
                                </div>
                            </div>
                            <div class="header">
                                <span class="from">Lukasz Holeczek</span>
                                <span class="date">
                                <span class="fa fa-paper-clip"></span> Today, 3:47 PM</span>
                            </div>
                            <div class="title">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                            <div class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </div>
                        </a>
                    </li>
                </ul>
            </main>
        </div>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
@endsection