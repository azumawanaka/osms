@extends('pages.students.layouts')

@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    @include('inc.students.topbar')
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Announcements</h1>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-bullhorn"></i>
                    @php
                        $title = 'THE MORNING ROUTINE';
                    @endphp
                    {{$title}}
                    <small class="text-muted pull-right">1 day ago </small>
                </h6>
            </div>
            <div class="card-body">
                <div class="announcement_inner">
                    {{-- {{ substr_replace($your_text, "...", 20) }} --}}
                    Goooooooooooood morning, Warriors! That get-ready-for-the-day call is heard each morning by 850 students at Goodwyn Junior High School in Montgomery, Alabama. The familiar intercom announcement, a reference to the school's mascot, is echoed each day by principal Marie Kostick as she psyches up -- and wakes up! -- her charges.<br><br>
                    After Kostick's wake-up call, members of the school's student council take over the daily routine, which continues with the Pledge of Allegiance, a moment of silence, and a thought for the day. "We use student council representatives to relay these messages because we think the student body might listen more closely and buy into messages more readily when they come from their peers," said Kostick, who often jumps in to close the a.m. ritual with an upbeat message, such as congratulations for a team victory or for another special accomplishment.<br><br>
                    At Doctors Inlet Elementary School in Middleburg, Florida, morning announcements, which are presented over closed-circuit TV, follow a pretty standard format too. The student-anchors of the school's on-camera news team welcome their peers to a new school day, then lead the Pledge of Allegiance and a good-morning "America" sing-along. Anchors announce school-wide events, famous-people birthdays, the lunch menu, and the weather report.<br><br>
                    After the formalities are out of the way, the anchor introduces a special "Guest of the Day." On Monday and Thursday, that special guest is the school's assistant principal, Maureen Yelverton, whose twice-a-week talks revolve around one of the school's biggest thrusts, its reading curriculum. Yelverton also draws names for prizes awarded as part of the school's Reading Counts program. Wednesday's guest is school nurse, Judy LaRue, who offers health tidbits for students. On Tuesdays and Fridays, principal Larry Davis presents Mr. Davis's Math Question. "The first person in each class to correctly solve the math problem races down to the office to receive his or her special 'Mr. Davis pencil,'" Davis told Education World.<br><br>
                    "Of course," he added, "I give teachers the answers in advance."<br><br>
                    A sampling of "Mr. Davis's Math Questions" appears later in this article.
                    <a href="{{ url('students/announcements/') .'/'.$title }}" class="show_more text-muted d-flex align-items-center justify-content-center">
                        <span class="btn btn-primary btn-md">View Announcement</span>
                    </a>
                </div>

                <div class="mt-4 border-top pt-3">
                    <small class="text-muted">Author: Miss Jorlyn Misa</small>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-bullhorn"></i> THE MORNING ROUTINE
                    <small class="text-muted pull-right">1 day ago </small>
                </h6>
            </div>
            <div class="card-body">
                <div class="announcement_inner">
                    {{-- {{ substr_replace($your_text, "...", 20) }} --}}
                    Goooooooooooood morning, Warriors! That get-ready-for-the-day call is heard each morning by 850 students at Goodwyn Junior High School in Montgomery, Alabama. The familiar intercom announcement, a reference to the school's mascot, is echoed each day by principal Marie Kostick as she psyches up -- and wakes up! -- her charges.<br><br>
                    After Kostick's wake-up call, members of the school's student council take over the daily routine, which continues with the Pledge of Allegiance, a moment of silence, and a thought for the day. "We use student council representatives to relay these messages because we think the student body might listen more closely and buy into messages more readily when they come from their peers," said Kostick, who often jumps in to close the a.m. ritual with an upbeat message, such as congratulations for a team victory or for another special accomplishment.<br><br>
                    At Doctors Inlet Elementary School in Middleburg, Florida, morning announcements, which are presented over closed-circuit TV, follow a pretty standard format too. The student-anchors of the school's on-camera news team welcome their peers to a new school day, then lead the Pledge of Allegiance and a good-morning "America" sing-along. Anchors announce school-wide events, famous-people birthdays, the lunch menu, and the weather report.<br><br>
                    After the formalities are out of the way, the anchor introduces a special "Guest of the Day." On Monday and Thursday, that special guest is the school's assistant principal, Maureen Yelverton, whose twice-a-week talks revolve around one of the school's biggest thrusts, its reading curriculum. Yelverton also draws names for prizes awarded as part of the school's Reading Counts program. Wednesday's guest is school nurse, Judy LaRue, who offers health tidbits for students. On Tuesdays and Fridays, principal Larry Davis presents Mr. Davis's Math Question. "The first person in each class to correctly solve the math problem races down to the office to receive his or her special 'Mr. Davis pencil,'" Davis told Education World.<br><br>
                    "Of course," he added, "I give teachers the answers in advance."<br><br>
                    A sampling of "Mr. Davis's Math Questions" appears later in this article.
                    <a href="#" class="show_more text-muted d-flex align-items-center justify-content-center">
                        <span class="btn btn-primary btn-md">View Announcement</span>
                    </a>
                </div>

                <div class="mt-4 border-top pt-3">
                    <small class="text-muted">Author: Miss Jorlyn Misa</small>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-bullhorn"></i> THE MORNING ROUTINE
                    <small class="text-muted pull-right">1 day ago </small>
                </h6>
            </div>
            <div class="card-body">
                <div class="announcement_inner">
                    {{-- {{ substr_replace($your_text, "...", 20) }} --}}
                    Goooooooooooood morning, Warriors! That get-ready-for-the-day call is heard each morning by 850 students at Goodwyn Junior High School in Montgomery, Alabama. The familiar intercom announcement, a reference to the school's mascot, is echoed each day by principal Marie Kostick as she psyches up -- and wakes up! -- her charges.<br><br>
                    After Kostick's wake-up call, members of the school's student council take over the daily routine, which continues with the Pledge of Allegiance, a moment of silence, and a thought for the day. "We use student council representatives to relay these messages because we think the student body might listen more closely and buy into messages more readily when they come from their peers," said Kostick, who often jumps in to close the a.m. ritual with an upbeat message, such as congratulations for a team victory or for another special accomplishment.<br><br>
                    At Doctors Inlet Elementary School in Middleburg, Florida, morning announcements, which are presented over closed-circuit TV, follow a pretty standard format too. The student-anchors of the school's on-camera news team welcome their peers to a new school day, then lead the Pledge of Allegiance and a good-morning "America" sing-along. Anchors announce school-wide events, famous-people birthdays, the lunch menu, and the weather report.<br><br>
                    After the formalities are out of the way, the anchor introduces a special "Guest of the Day." On Monday and Thursday, that special guest is the school's assistant principal, Maureen Yelverton, whose twice-a-week talks revolve around one of the school's biggest thrusts, its reading curriculum. Yelverton also draws names for prizes awarded as part of the school's Reading Counts program. Wednesday's guest is school nurse, Judy LaRue, who offers health tidbits for students. On Tuesdays and Fridays, principal Larry Davis presents Mr. Davis's Math Question. "The first person in each class to correctly solve the math problem races down to the office to receive his or her special 'Mr. Davis pencil,'" Davis told Education World.<br><br>
                    "Of course," he added, "I give teachers the answers in advance."<br><br>
                    A sampling of "Mr. Davis's Math Questions" appears later in this article.
                    <a href="#" class="show_more text-muted d-flex align-items-center justify-content-center">
                        <span class="btn btn-primary btn-md">View Announcement</span>
                    </a>
                </div>

                <div class="mt-4 border-top pt-3">
                    <small class="text-muted">Author: Miss Jorlyn Misa</small>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-bullhorn"></i> THE MORNING ROUTINE
                    <small class="text-muted pull-right">1 day ago </small>
                </h6>
            </div>
            <div class="card-body">
                <div class="announcement_inner">
                    {{-- {{ substr_replace($your_text, "...", 20) }} --}}
                    Goooooooooooood morning, Warriors! That get-ready-for-the-day call is heard each morning by 850 students at Goodwyn Junior High School in Montgomery, Alabama. The familiar intercom announcement, a reference to the school's mascot, is echoed each day by principal Marie Kostick as she psyches up -- and wakes up! -- her charges.<br><br>
                    After Kostick's wake-up call, members of the school's student council take over the daily routine, which continues with the Pledge of Allegiance, a moment of silence, and a thought for the day. "We use student council representatives to relay these messages because we think the student body might listen more closely and buy into messages more readily when they come from their peers," said Kostick, who often jumps in to close the a.m. ritual with an upbeat message, such as congratulations for a team victory or for another special accomplishment.<br><br>
                    At Doctors Inlet Elementary School in Middleburg, Florida, morning announcements, which are presented over closed-circuit TV, follow a pretty standard format too. The student-anchors of the school's on-camera news team welcome their peers to a new school day, then lead the Pledge of Allegiance and a good-morning "America" sing-along. Anchors announce school-wide events, famous-people birthdays, the lunch menu, and the weather report.<br><br>
                    After the formalities are out of the way, the anchor introduces a special "Guest of the Day." On Monday and Thursday, that special guest is the school's assistant principal, Maureen Yelverton, whose twice-a-week talks revolve around one of the school's biggest thrusts, its reading curriculum. Yelverton also draws names for prizes awarded as part of the school's Reading Counts program. Wednesday's guest is school nurse, Judy LaRue, who offers health tidbits for students. On Tuesdays and Fridays, principal Larry Davis presents Mr. Davis's Math Question. "The first person in each class to correctly solve the math problem races down to the office to receive his or her special 'Mr. Davis pencil,'" Davis told Education World.<br><br>
                    "Of course," he added, "I give teachers the answers in advance."<br><br>
                    A sampling of "Mr. Davis's Math Questions" appears later in this article.
                    <a href="#" class="show_more text-muted d-flex align-items-center justify-content-center">
                        <span class="btn btn-primary btn-md">View Announcement</span>
                    </a>
                </div>

                <div class="mt-4 border-top pt-3">
                    <small class="text-muted">Author: Miss Jorlyn Misa</small>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-bullhorn"></i> THE MORNING ROUTINE
                    <small class="text-muted pull-right">1 day ago </small>
                </h6>
            </div>
            <div class="card-body">
                <div class="announcement_inner">
                    {{-- {{ substr_replace($your_text, "...", 20) }} --}}
                    Goooooooooooood morning, Warriors! That get-ready-for-the-day call is heard each morning by 850 students at Goodwyn Junior High School in Montgomery, Alabama. The familiar intercom announcement, a reference to the school's mascot, is echoed each day by principal Marie Kostick as she psyches up -- and wakes up! -- her charges.<br><br>
                    After Kostick's wake-up call, members of the school's student council take over the daily routine, which continues with the Pledge of Allegiance, a moment of silence, and a thought for the day. "We use student council representatives to relay these messages because we think the student body might listen more closely and buy into messages more readily when they come from their peers," said Kostick, who often jumps in to close the a.m. ritual with an upbeat message, such as congratulations for a team victory or for another special accomplishment.<br><br>
                    At Doctors Inlet Elementary School in Middleburg, Florida, morning announcements, which are presented over closed-circuit TV, follow a pretty standard format too. The student-anchors of the school's on-camera news team welcome their peers to a new school day, then lead the Pledge of Allegiance and a good-morning "America" sing-along. Anchors announce school-wide events, famous-people birthdays, the lunch menu, and the weather report.<br><br>
                    After the formalities are out of the way, the anchor introduces a special "Guest of the Day." On Monday and Thursday, that special guest is the school's assistant principal, Maureen Yelverton, whose twice-a-week talks revolve around one of the school's biggest thrusts, its reading curriculum. Yelverton also draws names for prizes awarded as part of the school's Reading Counts program. Wednesday's guest is school nurse, Judy LaRue, who offers health tidbits for students. On Tuesdays and Fridays, principal Larry Davis presents Mr. Davis's Math Question. "The first person in each class to correctly solve the math problem races down to the office to receive his or her special 'Mr. Davis pencil,'" Davis told Education World.<br><br>
                    "Of course," he added, "I give teachers the answers in advance."<br><br>
                    A sampling of "Mr. Davis's Math Questions" appears later in this article.
                    <a href="#" class="show_more text-muted d-flex align-items-center justify-content-center">
                        <span class="btn btn-primary btn-md">View Announcement</span>
                    </a>
                </div>

                <div class="mt-4 border-top pt-3">
                    <small class="text-muted">Author: Miss Jorlyn Misa</small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
@endsection