@extends('pages.students.layouts')

@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    @include('inc.students.topbar')
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
        
        @include('pages.alert-content')

        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Assignments</h1>
        
        <div class="mb-4 border-top border-bottom pt-1 pb-2">
            Color Indicator<br>
            <small class="badge badge-success">Answered</small>
            <small class="badge badge-warning">No Answer</small>
            <small class="badge badge-danger">Assignment Expired and no answer</small>
        </div>
        <div class="mb-5">
            @php
                $counter = 0;
            @endphp
            @foreach ($assignments as $assign) 
                @php
                    $counter++;
                @endphp
                
                @php
                    $answered = false;
                    $ans_content = "";
                @endphp
                @if ($assign->assignment_answers->users_id == 2 && $assign->assignment_answers->assignments_id == $assign->id)
                    @php
                        $answered = true;
                        $ans_content = $assign->assignment_answers->answer;
                    @endphp
                @endif
                <div class="accordion shadow" id="assignment_{{$assign->id}}">
                    <div class="card">
                        <div class="card-header p-3" id="heading_{{$assign->id}}" type="button"  data-toggle="collapse" data-target="#collapse_{{$assign->id}}" aria-expanded="true" aria-controls="collapse_{{$assign->id}}">
                            <h6 class="mb-0 pr-5">
                                <span class="badge @if ($answered)badge-success @else badge-warning @endif p-2 mr-2" @if ($answered) data-tooltip="tooltip" title="Answered" @endif>
                                    {{$assign->subjects->subject_name}}
                                </span>
                                {{$assign->title}}
                                <small class="pull-right text-muted">
                                    {{$assign->updated_at->diffForHumans()}}
                                </small>
                            </h6>
                        </div>
                        <div id="collapse_{{$assign->id}}" class="collapse" aria-labelledby="heading_{{$assign->id}}" data-parent="#assignment_{{$assign->id}}">
                            <div class="card-body">
                                {!!nl2br($assign->content)!!}
                                <div class="mt-5 border-top pt-3 text-right">
                                    @if ($answered)
                                        <a href="#" class="btn btn-md btn-outline-success" data-toggle="modal" data-target="#assignment_modal_{{$assign->id}}">Check My Answer</a>
                                    @else
                                        <a href="#" class="btn btn-md btn-outline-primary" data-toggle="modal" data-target="#assignment_modal_{{$assign->id}}">Answer Now</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="assignment_modal_{{$assign->id}}" tabindex="-1" role="dialog" aria-labelledby="a_modal_label_{{$assign->id}}" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="a_modal_label_{{$assign->id}}">{{$assign->title}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            @if (!$answered)
                            <form class="form" action="{{route('students.assignments.answer')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                            @endif
                                <input type="hidden" name="assignments_id" value="{{$assign->id}}">
                                <div class="modal-body">
                                    <div class="form-group">
                                        @if ($answered)
                                            {!!nl2br($ans_content)!!}
                                        @else 
                                            <textarea name="assignment_answer" id="assignment_answer" cols="30" rows="14" class="form-control" placeholder="Your answers here...">{{ $assign->content }}</textarea>
                                        @endif
                                    </div>
                                    @if (!$answered)
                                    <div class="form-group">
                                        <label for="files" class="d-block"> Want to add files? </label>
                                        <input type="file" name="files[]" multiple>
                                    </div>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    @if ($answered)
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    @else
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary">Submit Answer</button>
                                    @endif
                                </div>
                            @if (!$answered)
                            </form>
                            @endif
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
        {{-- paginate --}}
        <div class="d-flex justify-content-end">
            <?php echo $assignments->render(); ?>
        </div>
        {{-- //paginate --}}
        
    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
@endsection