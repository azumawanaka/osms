@extends('pages.students.layouts')

@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    @include('inc.students.topbar')
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">
		
        <ol class="breadcrumb">
			<?php $segments = ''; ?>
			@foreach(Request::segments() as $segment)
				<?php $segments .= '/'.$segment; ?>
				<li>
					@if ( $segment == $title )
						<span class="text-muted">
							<small>{{ ucfirst($segment) }}</small>
						</span>
					@else
						<a href="{{ $segments }}" class="text-info">
							<small>{{ ucfirst($segment) }} &nbsp;<i class="fa fa-chevron-right"></i> &nbsp;</small>
						</a>
					@endif
				</li>
			@endforeach
		</ol>
        <!-- Page Heading -->
        <div class="announcement-body shadow pl-5 pr-5 pt-4 pb-5 mb-5">
			<h3 class="border-bottom pb-2">
			  <i class="fa fa-bullhorn text-info"></i> {{$title}}
			</h3>
			<small class="d-block mb-4 text-muted">1 day ago </small>
			<p>
				Goooooooooooood morning, Warriors! That get-ready-for-the-day call is heard each morning by 850 students at Goodwyn Junior High School in Montgomery, Alabama. The familiar intercom announcement, a reference to the school's mascot, is echoed each day by principal Marie Kostick as she psyches up -- and wakes up! -- her charges.<br><br>
				After Kostick's wake-up call, members of the school's student council take over the daily routine, which continues with the Pledge of Allegiance, a moment of silence, and a thought for the day. "We use student council representatives to relay these messages because we think the student body might listen more closely and buy into messages more readily when they come from their peers," said Kostick, who often jumps in to close the a.m. ritual with an upbeat message, such as congratulations for a team victory or for another special accomplishment.<br><br>
				At Doctors Inlet Elementary School in Middleburg, Florida, morning announcements, which are presented over closed-circuit TV, follow a pretty standard format too. The student-anchors of the school's on-camera news team welcome their peers to a new school day, then lead the Pledge of Allegiance and a good-morning "America" sing-along. Anchors announce school-wide events, famous-people birthdays, the lunch menu, and the weather report.<br><br>
				After the formalities are out of the way, the anchor introduces a special "Guest of the Day." On Monday and Thursday, that special guest is the school's assistant principal, Maureen Yelverton, whose twice-a-week talks revolve around one of the school's biggest thrusts, its reading curriculum. Yelverton also draws names for prizes awarded as part of the school's Reading Counts program. Wednesday's guest is school nurse, Judy LaRue, who offers health tidbits for students. On Tuesdays and Fridays, principal Larry Davis presents Mr. Davis's Math Question. "The first person in each class to correctly solve the math problem races down to the office to receive his or her special 'Mr. Davis pencil,'" Davis told Education World.<br><br>
				"Of course," he added, "I give teachers the answers in advance."<br><br>
				A sampling of "Mr. Davis's Math Questions" appears later in this article.
			</p>
			<div class="mt-4 border-top pt-3">
				<small class="text-muted">Author: Miss Jorlyn Misa</small>
				<h6 class="pull-right">
					<i class="fa fa-comments text-info"> 5</i>&nbsp;
					<i class="fa fa-eye text-primary"> 50</i>
				</h6>
			</div>
		</div>

		{{-- form --}}
		<h4>Comment Section</h4>
		<form action="#" class="form" method="POST">
			@csrf
			<div class="form-group">
				<textarea name="comment" id="comment" cols="30" rows="5" class="form-control" placeholder="Dear Miss Jorlyn ...."></textarea>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary btn-md" value="Submit">
			</div>
		</form>

    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- End of Main Content -->
@endsection