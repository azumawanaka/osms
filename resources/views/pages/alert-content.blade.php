
@if (isset(Session::get('message')['type']))
    @component('pages.alert')
        @slot('type')
            {{Session::get('message')['type']}}
        @endslot

        @slot('title')
            {{Session::get('message')['title']}}
        @endslot
        {{Session::get('message')['msg']}}
    @endcomponent
@endif