<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('users_id')->unsigned();
            $table->bigInteger('teachers_id')->unsigned();
            $table->char('firstname', 150)->nullable();
            $table->char('middlename', 150)->nullable();
            $table->char('lastname',150)->nullable();
            $table->tinyInteger('age')->default(4);
            $table->char('sex',50)->default('M');
            $table->char('cell_tel', 250);
            $table->char('nationality',250);
            $table->text('permanent_address');
            $table->text('current_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
