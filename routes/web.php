<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Students announcements
Route::get('students/announcements','AnnouncementsController@index')->name('students.home');
Route::get('students/announcements/{id}','AnnouncementsController@show');

// Students assignments
Route::get('students/assignments','AssignmentsController@index')->name('students.assignments');
Route::post('students/assignments/answer/','AssignmentsController@store')->name('students.assignments.answer');

// Emails
Route::get('email/inbox','EmailsController@index')->name('emails.inbox');

Route::redirect('students', '/students/announcements');
// middleware
Route::group(['middleware' => ['web']], function(){
    // Route::get('students', 'AnnouncementsController');
});
