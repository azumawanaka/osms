
Project Name: Online Student Monitoring System

General Functionalities: 
    > The system is use to monitor student's academic performance, lessons and project accomplishment. 
    > The system will also let the student's guardian monitor their student's all performance.
    > The system will let the teacher send reports, set assignments, grades ( based on student's performance ), 
        school news or school announcements. 
    > The system can track overall student's activities like school projects, attendance and behavioral.
    > The system will send/recieve emails or sms notifications from/to guardians, students, and teachers.
    > The system can generate monthly reports it depends on the teacher's given settings.
    > The system will have Private and Group Messages.

Users : [ Teacher, Student, Guardian, Admin ]

Roles:
    1. Admin
        1-1. Login/Logout
        1-2. Register Teachers
        1-3. View teacher's activities
        1-4. Email/SMS notifications
        1-5. Publish school news
        1-6. Export reports from teacher's activities
        1-7. View complaints from teachers and/or guardians
        1-8. View teacher's student informations
        1-9. Update profile informations
        1-10. Group meetings or group chat with the teachers and parents
        1-11. Attend group meetings
    
    2. Teachers
        2-1. Login/Logout
        2-2. Register students and guardians
        2-3. Add exams, assignments, projects and activities
        2-4. Generate reports of student's grades, performances and assignments
        2-5. Publish school news or announcements
        2-6. Email/SMS notifications
        2-7. View student's informations
        2-8. Send/View complaints from guardians and students
        2-9. Create subjects
        2-10. Update profile informations
        2-11. Create group chat for guardians and students
        2-12. Attend group meetings
    
    3. Students
        3-1. Login/Logout
        3-2. View and answer from given tasks
        3-3. Email/SMS notifications
        3-4. Update profile informations
        3-5. View and comment to teacher's announcements/news
        3-6. View grades
        3-7. Generate reports

    4. Guardians
        4-1. Login/Logout
        4-2. View his/her student's activities
        4-3. Generate reports
        4-4. Send/View complaints from teachers
        4-5. Email notifications
        4-6. View and comment to teacher's announcements/news
        4-7. Attend group meetings


Pages

Students
    1-1. Signin
    1-2. Dashboard
    1-3. News/Announcements
    1-4. Reports
    1-5. Activities
        1-5-1. Assignments
        1-5-2. Projects 
        1-5-3. Exams
        1-5-4. Attendance
    1-6. Messages
    1-7. Email notifications
    1-8. Profile Page
    1-9. Subject Lists
    1-10. Search 
    1-11. Forgot Password

Guardians
    1-1. Signin
    1-2. Dashboard
    1-3. News/Announcements
    1-4. Reports
    1-5. Messages 
    1-6. Child's personal informations
    1-7. Profile Page
    1-8. Search
    1-9. Email notifications
    1-10. Forgot Password

Teachers
    1-1. Signin
    1-2. Dashboard
    1-3. Students
    1-4. Reports
    1-5. Activities
        1-5-1. Assignments
        1-5-2. Projects 
        1-5-3. Exams
        1-5-4. Attendance
    1-6. News/Announcements
    1-7. Email notifications
    1-8. Complaints Page
    1-9. subjects
    1-10. Profile Page
    1-11. Messages
    1-12. Meetings
    1-13. Settings
    1-14. Forgot Password


Admin
    1-1. Signin
    1-2. Dashboard
    1-3. Teachers
    1-4. Reports
    1-5. News/Announcements
    1-6. Teacher's activities
    1-7. Complaints
    1-8. Student's informations
    1-9. Profile
    1-10. Email notifications
    1-11. Meetings
    1-12. Messages
    1-13. Forgot Password


Input Fields

Students
    1. Profile
        1-1. Profile Picture
        1-2. First Name
        1-3. Middle Name
        1-4. Last Name
        1-5. Age 
        1-6. Sex
        1-7. Email Address
        1-8. Cell/Tel No.
        1-9. Nationality
        1-10. Present Address ( Province/City/Zip Code )
        1-11. Current Address ( Province/City/Zip Code )