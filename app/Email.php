<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    private $fillable = [
        'users_id',
        'recipient',
        'subject',
        'body',
        'files'
    ];
}
