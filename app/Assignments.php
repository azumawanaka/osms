<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Model;
use App\AssignmentAnswers;
use App\Subjects;

class Assignments extends Model
{
    protected $fillable = [
        'users_id',
        'subjects_id',
        'title',
        'content',
        'files',
        'expired_on'
    ];
    
    public function subjects(): BelongsTo
    {
        return $this->belongsTo(Subjects::class);
    }

    public function assignment_answers(): HasOne
    {
        return $this->hasOne(AssignmentAnswers::class);
    }
}
