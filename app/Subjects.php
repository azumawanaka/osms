<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    protected $fillable = [
        'users_id',
        'subject_name',
        'notes'
    ];

}
