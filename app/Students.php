<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $fillable = [
        'user_id',
        'firstname', 
        'middlename', 
        'lastname',
        'age',
        'sex',
        'cell_tel',
        'nationality',
        'permanent_address',
        'current_address'
    ];
}
