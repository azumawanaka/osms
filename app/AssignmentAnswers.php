<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentAnswers extends Model
{
    protected $fillable = [
        'users_id',
        'assignments_id',
        'answer',
        'score',
        'notes'
    ];
}
