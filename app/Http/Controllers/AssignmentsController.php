<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Assignments;
use App\AssignmentAnswers;
use Redirect;

class AssignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $assignments = Assignments::with('subjects', 'assignment_answers')->paginate(12);
        
        $array = [
            'assignments' => $assignments,
        ];

        return view('pages.students.assignments', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $files = $request->file('files');
        $f = "";
        $uid = 2;
        if (!empty($files)) {
            $destinationPath = 'uploads/'; // upload path
            $f = $uid.'_'.date('mdY') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $f);
        }
        
        $insert_data = AssignmentAnswers::create([
            'users_id'          => $uid,
            'assignments_id'    => $request->input('assignments_id'),
            'answer'            => $request->input('assignment_answer'),
            'files'             => $f
        ]);

        if($insert_data) {
            $msg = array("type" => "success", "title" => "Success", "msg" => "Your answer was successfully submitted!");
        }else{
            $msg = array("type" => "danger", "title" => "Error", "msg" => "Something went wrong. Please try again later.");
        }

        return Redirect::back()->with('message', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
    }
}
